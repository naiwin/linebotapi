var ss = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/17_70ph_GewjPPYmFw8Mf38ymdGvrgT1-7-k3i82YfZY/edit");
var sheetCP = ss.getSheetByName("Sheet1");
var sheetDP = ss.getSheetByName("Sheet2");
function doPost(e) {
   
  var data = JSON.parse(e.postData.contents)
  var userMsg = data.originalDetectIntentRequest.payload.data.message.text;
  var cpAns = getSheet1(userMsg);
  var dpAns = getSheet2(userMsg);
  if (cpAns == null){
    value = dpAns;
  }else{
    value = cpAns;
  }
  if (value == null){
    return dPost("not found !!")
  }else{
    return dPost(value)
  }
  
}
function getSheet1(userMsg){
  var values = sheetCP.getRange(2, 1, sheetCP.getLastRow(),sheetCP.getLastColumn()).getValues();
for(var i = 0;i<values.length; i++){
    
    if(values[i][0] == userMsg ){
      i=i+2;
    var Data = sheetCP.getRange(i,2).getValue();
    return Data;
}
  }
}
function getSheet2(userMsg){
  var values = sheetDP.getRange(2, 1, sheetDP.getLastRow(),sheetDP.getLastColumn()).getValues();
for(var i = 0;i<values.length; i++){
    
    if(values[i][0] == userMsg ){
      i=i+2;
    var Data = sheetDP.getRange(i,2).getValue();
    return Data;
}
  }
}
function dPost(price){
  Data = price;
  var result = {"fulfillmentMessages": [{
      "platform": "line","type": 4,"payload" : {"line":  {"type": "text","text": Data}}}
    ]}
      
  var replyJSON = ContentService.createTextOutput(JSON.stringify(result)).setMimeType(ContentService.MimeType.JSON);
  return replyJSON;
}